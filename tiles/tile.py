from tiles.globals import TILE_SIZE

import pygame
from svg import Parser, Rasterizer


class Tile:
    """"""

    def __init__(self):
        self.crossable = False
        self.speed = 0

    def render(self, img="tiles/tile.svg"):
        buff = Rasterizer().rasterize(Parser.parse_file(img),
                                      TILE_SIZE, TILE_SIZE)
        return pygame.image.frombuffer(buff, (TILE_SIZE, TILE_SIZE), 'RGBA')
