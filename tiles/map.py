from tiles.entity import Entities
from tiles.globals import TILE_SIZE

import os
import pygame


class Map(dict):
    """"""

    def __init__(self, window, map={}, entities={}):
        pygame.init()
        self.window = window
        self.entities = Entities(window, entities)
        self.clear()
        dict.__init__(self, map)
        for pos, tile in self.items():
            self.window.blit(tile.render(), (pos[0] * TILE_SIZE,
                                             pos[1] * TILE_SIZE))
        pygame.display.flip()

    def __setitem__(self, key, value, no_render=False):
        dict.__setitem__(self, key, value)
        rect = self.window.blit(self[key].render(), (key[0] * TILE_SIZE,
                                                     key[1] * TILE_SIZE))
        if no_render:
            return rect
        pygame.display.update(rect)

    def __delitem__(self, key):
        dict.__delitem__(self, key)
        rect = self.window.fill((0, 0, 0), (key[0] * TILE_SIZE,
                                            key[1] * TILE_SIZE,
                                            TILE_SIZE, TILE_SIZE))
        pygame.display.update(rect)

    def __del__(self):
        try:
            self.clear()
            if "TILER_HIDE_MAP_DEL_MSG" not in os.environ.keys():
                print("Wowow! You deleted the map object on purpose!",
                      "Are you sure this wasn't an error? Consider using",
                      "map.clear() instead. If it wasn't an error, write this",
                      'somewhere: os.environ["TILER_HIDE_MAP_DEL_MSG"] = ""')
        except pygame.error:  # When closing the app, the deletion of all
            pass              # variable cause an error in the clear function
        del self

    def clear(self, from_xy=None, to_xy=None):
        if from_xy is not None and to_xy is not None:
            x = sorted((from_xy[0], to_xy[0]))
            y = sorted((from_xy[1], to_xy[1]))
            rect = self.window.fill((0, 0, 0), (x[0] * TILE_SIZE,
                                                y[0] * TILE_SIZE,
                                                x[1] * TILE_SIZE,
                                                y[1] * TILE_SIZE))
        else:
            rect = self.window.fill((0, 0, 0))
        pygame.display.update(rect)

    def spread(self, from_xy, to_xy, tile):
        x = sorted((from_xy[0], to_xy[0]))
        y = sorted((from_xy[1], to_xy[1]))
        rect = []
        x[1] = x[1] - x[0]
        y[1] = y[1] - y[0]
        i = 0
        j = 0
        while i <= x[1]:
            while j <= y[1]:
                rect.append(self.__setitem__((x[0] + i, y[0] + j), tile, True))
                j += 1
            i += 1
            j = 0
        pygame.display.update(rect)
