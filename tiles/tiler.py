from tiles.globals import WINDOW_WIDTH, WINDOW_HEIGHT
from tiles.map import Map
from tiles.tile import Tile

import pygame
from pygame.locals import QUIT, RESIZABLE, KEYUP
from threading import Thread


class Tiler:
    """"""

    def __init__(self, **args):
        pygame.init()
        self.window = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT),
                                              RESIZABLE)
        self.map = Map(self.window)
        self.map.spread((0, 0), (3, 3), Tile())

        self.quit = False
        loop = self.Loop(self)
        loop.start()

    class Loop(Thread):
        def __init__(self, tiler):
            Thread.__init__(self)
            self.tiler = tiler

        def run(self):
            while not self.tiler.quit:
                events = pygame.event.get()
                if events:
                    # TODO: Use walrus operator with python 3.8:
                    # if events := pygame.event.get():
                    self.tiler.action(events)

    def action(self, events):
        for e in events:
            if e.type == QUIT:
                self.quit = True
            if e.type == KEYUP and e.key == 101:
                self.map.spread((7, 7), (3, 3), Tile())
            if e.type == KEYUP and e.key == 100:
                self.map.clear((7, 7), (3, 3))
            if e.type == KEYUP and e.key == 107:
                self.map.clear()
