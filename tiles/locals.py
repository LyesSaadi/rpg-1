from tiles.globals import TILE_SIZE


class position:
    # HV position X_Y
    RIGHT_TOP = 11
    RIGHT_MIDDLE = 12
    RIGHT_BOTTOM = 13
    CENTER_TOP = 21
    CENTER_MIDDLE = 22
    CENTER_BOTTOM = 23
    LEFT_TOP = 31
    LEFT_MIDDLE = 32
    LEFT_BOTTOM = 33
    # VH position Y_X
    TOP_RIGHT = 11
    TOP_CENTER = 21
    TOP_LEFT = 31
    MIDDLE_RIGHT = 12
    MIDDLE_CENTER = 22
    MIDDLE_LEFT = 32
    BOTTOM_RIGHT = 13
    BOTTOM_CENTER = 23
    BOTTOM_LEFT = 33


class unit:
    def __init__(self, n, p):
        self.value = n
        self.position = p


def t(v, p):
    unit(v * TILE_SIZE, p)
